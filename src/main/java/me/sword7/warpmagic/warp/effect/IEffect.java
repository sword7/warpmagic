package me.sword7.warpmagic.warp.effect;

import org.bukkit.entity.Player;

public interface IEffect {

    void playParticles(Player player);

}
