package me.sword7.warpmagic;

import me.sword7.warpmagic.data.GlobalCache;
import me.sword7.warpmagic.data.UserCache;
import me.sword7.warpmagic.data.VortexCache;
import me.sword7.warpmagic.loot.CommandStone;
import me.sword7.warpmagic.loot.LootListener;
import me.sword7.warpmagic.loot.LootType;
import me.sword7.warpmagic.sys.PluginBase;
import me.sword7.warpmagic.sys.Version;
import me.sword7.warpmagic.sys.config.ConfigLoader;
import me.sword7.warpmagic.sys.config.PluginConfig;
import me.sword7.warpmagic.sys.lang.Language;
import me.sword7.warpmagic.util.AutoCompleteListener;
import me.sword7.warpmagic.warp.*;
import me.sword7.warpmagic.warp.extras.CommandBottom;
import me.sword7.warpmagic.warp.extras.CommandJump;
import me.sword7.warpmagic.warp.extras.CommandTop;
import me.sword7.warpmagic.warp.global.CommandDelPoint;
import me.sword7.warpmagic.warp.global.CommandSetPoint;
import me.sword7.warpmagic.warp.global.CommandToPoint;
import me.sword7.warpmagic.warp.global.GlobalPoint;
import me.sword7.warpmagic.warp.requests.CommandTPA;
import me.sword7.warpmagic.warp.requests.CommandTPAHere;
import me.sword7.warpmagic.warp.requests.CommandTPAccept;
import me.sword7.warpmagic.warp.requests.CommandTPDeny;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public final class WarpMagic extends JavaPlugin {

    private static Plugin plugin;

    @Override
    public void onEnable() {
        plugin = this;
        ConfigLoader.load();
        new PluginConfig();
        new Language();
        new GlobalCache();
        new UserCache();
        new VortexCache();
        new PluginBase();

        new Teleportation();
        new LootListener();
        new RespawnListener();
        if (Version.current.value >= 109) new AutoCompleteListener();

        getCommand("warpmagic").setExecutor(new CommandWarpMagic());
        getCommand("home").setExecutor(new CommandHome());
        getCommand("fhome").setExecutor(new CommandFHome());
        getCommand("warp").setExecutor(new CommandWarp());
        getCommand("vortex").setExecutor(new CommandVortex());
        getCommand("warpstone").setExecutor(new CommandStone(LootType.WARP_STONE.getLoot()));
        getCommand("vortexstone").setExecutor(new CommandStone(LootType.VORTEX_STONE.getLoot()));
        getCommand("spawn").setExecutor(new CommandToPoint(GlobalPoint.SPAWN));
        getCommand("hub").setExecutor(new CommandToPoint(GlobalPoint.HUB));
        getCommand("setspawn").setExecutor(new CommandSetPoint(GlobalPoint.SPAWN));
        getCommand("sethub").setExecutor(new CommandSetPoint(GlobalPoint.HUB));
        getCommand("delspawn").setExecutor(new CommandDelPoint(GlobalPoint.SPAWN));
        getCommand("delhub").setExecutor(new CommandDelPoint(GlobalPoint.HUB));
        getCommand("tpa").setExecutor(new CommandTPA());
        getCommand("tpahere").setExecutor(new CommandTPAHere());
        getCommand("tpaccept").setExecutor(new CommandTPAccept());
        getCommand("tpdeny").setExecutor(new CommandTPDeny());
        getCommand("jump").setExecutor(new CommandJump());
        getCommand("top").setExecutor(new CommandTop());
        getCommand("bottom").setExecutor(new CommandBottom());

    }

    @Override
    public void onDisable() {
        UserCache.save();
        VortexCache.save();
        GlobalCache.save();
        PluginBase.disableDependencies();
    }

    public static Plugin getPlugin() {
        return plugin;
    }
}
